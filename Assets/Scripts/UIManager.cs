﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Progressbar")]
    public Transform ProgressBarRoot;
    public GameObject ProgressBarPrefab;

    [Header("Text Animation")]
    public float secondsPerOneHundred;
    [Header("Money")]
    public Text MoneyText;
    public Coroutine MoneyCoroutine;
    [Header("Spot Price")]
    public Text SpotPriceText;
    public Coroutine SpotPriceCoroutine;
    [Header("Navigation Buttons")]
    public Button MainMenu;
    public Button Retry;
    [Header("Lose Screen")]
    public GameObject LoseScreen;
    public Text ScoreText;
    public GameObject HighscoreField;

    private RoundManager roundManager;
    private GameManager gm;

    // Use this for initialization
    private void Start()
    {
        roundManager = GetComponent<RoundManager>();
        roundManager.MoneyChanged += UpdateMoney;
        UpdateMoney(0);
        roundManager.SpotPriceChanged += UpdateSpotPrice;
        UpdateSpotPrice(roundManager.SpotUnlockPrice);

        SetupProgressbars();


        gm = FindObjectOfType<GameManager>();
        MainMenu.onClick.AddListener(gm.ToMainMenu);
        Retry.onClick.AddListener(gm.StartGame);
    }

    private void SetupProgressbars()
    {
        ServiceSpot[] serviceSpots = FindObjectsOfType<ServiceSpot>();
        foreach (var serviceSpot in serviceSpots)
        {
            GameObject progressBar = Instantiate(ProgressBarPrefab, ProgressBarRoot);
            progressBar.GetComponent<WorldLockedUI>().WorldPosition = serviceSpot.transform.position + serviceSpot.transform.up;

            progressBar.GetComponent<Progressbar>().SetSpot(serviceSpot);
        }
    }
    private void UpdateMoney(int money)
    {
        if (MoneyCoroutine != null)
        {
            StopCoroutine(MoneyCoroutine);
            MoneyCoroutine = null;
        }
        MoneyCoroutine = StartCoroutine(AnimateText(roundManager.Money, money, MoneyText));
    }

    private IEnumerator AnimateText(int from, int to, Text textField)
    {
        int difference = to - from;
        int value = from;
        float startTime = Time.time;
        float neededTime = Mathf.Abs(difference / 100f * secondsPerOneHundred);

        while (value != to)
        {
            yield return null;

            value += (int)(difference / neededTime * (Time.time - startTime));
            if (difference > 0)
            {
                if (value > to)
                {
                    value = to;
                }
            }
            else
            {
                if (value < to)
                {
                    value = to;
                }
            }

            textField.text = value.ToString();
        }

        textField.text = value.ToString();
    }

    private void UpdateSpotPrice(int price)
    {

        if (SpotPriceCoroutine != null)
        {
            StopCoroutine(SpotPriceCoroutine);
            SpotPriceCoroutine = null;
        }
        SpotPriceCoroutine = StartCoroutine(AnimateText(roundManager.SpotUnlockPrice, price, SpotPriceText));
    }


    public void LostGame(int score)
    {
        ScoreText.text = score.ToString();
        if (gm.IsHighscore(score))
        {
            HighscoreField.SetActive(true);
        }

        LoseScreen.SetActive(true);
    }
}
