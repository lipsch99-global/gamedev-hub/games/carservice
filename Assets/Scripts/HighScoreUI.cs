﻿using UnityEngine;
using UnityEngine.UI;

public class HighScoreUI : MonoBehaviour
{
    [SerializeField]
    private Text[] ScoreTexts = default;
    private GameManager gm;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();

        int[] scores = gm.GetScores();

        for (int i = 0; i < 10; i++)
        {
            string text = (i + 1).ToString();
            text = text.PadLeft(2, ' ');
            text += ": ";
            string number = string.Format("{0,4}", scores[i]);
            text += number;
            ScoreTexts[i].text = text;
        }

    }

}
