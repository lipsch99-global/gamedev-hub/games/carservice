﻿using System;
using UnityEngine;

public class ServiceSpot : ParkingSpot
{
    [SerializeField]
    private Service _ServiceType = default;

    public Service ServiceType
    {
        get
        {
            return _ServiceType;
        }
    }

    private float serviceStartTime;

    public Action<ServiceSpot, float> ProgressChanged;

    private bool serviceDone = false;

    private void Update()
    {
        if (AssignedCar != null)
        {
            ProgressChanged?.Invoke(this, (Time.time - serviceStartTime) / _ServiceType.ServiceDuration);
            if (Time.time - serviceStartTime >= ServiceType.ServiceDuration && serviceDone == false)
            {
                AssignedCar.ServiceDone(ServiceType);
                serviceDone = true;
            }
        }
        if (AssignedCar == null)
        {
            ProgressChanged?.Invoke(this, 0);
        }
    }

    public override bool CanCarGetDropped(Car car)
    {
        bool canGetDropped = base.CanCarGetDropped(car);
        if (car.GetNeededService() == _ServiceType)
        {
            return canGetDropped && true;
        }
        return false;
    }

    public override void CarAssigned()
    {
        serviceStartTime = Time.time;
        serviceDone = false;
    }
}
