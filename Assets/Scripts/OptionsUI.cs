﻿using UnityEngine;

public class OptionsUI : MonoBehaviour
{
    [SerializeField]
    private Animator animator;


    private bool isUp = true;

    // Use this for initialization
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Down()
    {
        animator.SetTrigger("Down");
        isUp = false;
    }

    private void Up()
    {
        animator.SetTrigger("Up");
        isUp = true;
    }


    public void Toggle()
    {
        if (isUp)
        {
            Down();
        }
        else
        {
            Up();
        }
    }

    public void ToggleSound()
    {
        FindObjectOfType<SoundManager>().ToggleSound();
    }

    public void GoToMenu()
    {
        FindObjectOfType<GameManager>().ToMainMenu();
    }
}
