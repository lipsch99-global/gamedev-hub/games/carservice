﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private enum SceneType
    {
        MainMenu, Game
    }

    private RoundManager roundManagerOfActiveGame;

    private void Awake()
    {
        if (FindObjectsOfType<GameManager>().Length > 1)
        {
            DestroyImmediate(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += LinkSceneDependentStuff;
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);

    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    private void LinkSceneDependentStuff(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == 0)
        {
            GameObject.FindGameObjectWithTag("ChangeSceneButton").GetComponent<Button>().onClick.AddListener(StartGame);
        }
        else
        {
            roundManagerOfActiveGame = FindObjectOfType<RoundManager>();
            roundManagerOfActiveGame.GameOver += GameOver;
        }

    }


    private void GameOver(int score)
    {
        AddScore(score);
    }

    public int[] GetScores()
    {
        int[] Scores = new int[10];
        for (int i = 0; i < 10; i++)
        {
            Scores[i] = PlayerPrefs.GetInt("HighScore" + (i + 1).ToString("00"), 0);
        }
        return Scores;
    }

    private void AddScore(int newScore)
    {
        List<int> scores = GetScores().ToList();

        scores.Add(newScore);

        scores.Sort((x, y) => y.CompareTo(x));

        scores.RemoveAt(10);

        for (int i = 0; i < 10; i++)
        {
            PlayerPrefs.SetInt("HighScore" + (i + 1).ToString("00"), scores[i]);
        }

    }

    public bool IsHighscore(int score)
    {
        int[] scores = GetScores();
        if (scores.Max() <= score)
        {
            return true;
        }
        return false;
    }

    [ContextMenu("Reset Scores")]
    private void ResetScores()
    {
        PlayerPrefs.DeleteAll();
    }



}
