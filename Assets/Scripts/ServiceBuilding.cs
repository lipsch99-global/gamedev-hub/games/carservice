﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ServiceBuilding : MonoBehaviour
{
    [SerializeField]
    private ParkingSpot[] ParkingSpots = default;

    private RoundManager roundManager;

    private void Start()
    {
        roundManager = FindObjectOfType<RoundManager>();
    }

    public bool UnlockSpot()
    {
        if (roundManager.Money >= roundManager.SpotUnlockPrice)
        {
            foreach (var spot in ParkingSpots)
            {
                if (spot.Unlocked == false)
                {
                    roundManager.BoughtSpot();
                    spot.UnlockSpot();
                    return true;
                }
            }
        }
        return false;
    }
}
