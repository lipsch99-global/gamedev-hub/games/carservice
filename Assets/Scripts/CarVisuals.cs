﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CarVisuals : MonoBehaviour
{
    [SerializeField]
    private Transform topBorder = default;
    [SerializeField]
    private Transform botBorder = default;

    [SerializeField]
    private GameObject CarPrefab = default;

    [SerializeField]
    private Transform topSpawn = default;
    [SerializeField]
    private Transform botSpawn = default;
    [SerializeField]
    private float carSpeed = default;

    private List<Transform> carPool;

    // Use this for initialization
    private void Start()
    {
        carPool = new List<Transform>();
        for (int i = 0; i < 3; i++)
        {
            GameObject go = Instantiate(CarPrefab);


            Destroy(go.GetComponent<Car>());
            Destroy(go.GetComponent<BoxCollider2D>());
            Destroy(go.transform.Find("NeedIcon").gameObject);
            go.transform.Find("Particle System").gameObject.SetActive(true);


            carPool.Add(go.transform);
            go.transform.position = new Vector3(0, 500, go.transform.position.z);
        }
    }

    // Update is called once per frame
    private void Update()
    {
        foreach (var car in carPool)
        {
            if (car.transform.position.y > topBorder.transform.position.y ||
                car.transform.position.y < botBorder.transform.position.y)
            {
                if (Random.value > 0.5)
                {
                    car.position = topSpawn.position + new Vector3(0, Random.Range(0, 20), 0);
                    if (carPool.Any(x => Mathf.Abs(x.position.y - car.position.y) < 3 && x != car))
                    {
                        car.position += new Vector3(0, 3, 0);
                    }
                    car.rotation = topSpawn.rotation;
                }
                else
                {
                    car.position = botSpawn.position - new Vector3(0, Random.Range(0, 20), 0);
                    if (carPool.Any(x => Mathf.Abs(x.position.y - car.position.y) < 3 && x != car))
                    {
                        car.position -= new Vector3(0, 3, 0);
                    }
                    car.rotation = botSpawn.rotation;
                }
            }


            car.position += car.up * carSpeed * Time.deltaTime;
        }
    }
}
