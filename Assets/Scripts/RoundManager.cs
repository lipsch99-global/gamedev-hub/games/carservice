﻿using System;
using System.Linq;
using UnityEngine;

public class RoundManager : MonoBehaviour
{
    public Action<int> GameOver;
    public Action<int> MoneyChanged;
    public Action<int> SpotPriceChanged;
    public Action CarLeft;

    private int totalEarnedMoney;

    private int _Money;
    public int Money
    {
        get
        {
            return _Money;
        }
        private set
        {
            if (value > _Money)
            {
                int addedMoney = value - _Money;
                totalEarnedMoney += addedMoney;
            }
            MoneyChanged?.Invoke(value);
            _Money = value;

        }
    }

    private int spotsUnlocked = 0;

    [SerializeField]
    private int SpotUnlockPriceIncrease = default;

    private UIManager uiManager;

    public int SpotUnlockPrice
    {
        get
        {
            return (spotsUnlocked + 1) * SpotUnlockPriceIncrease;
        }
    }

    private void Start()
    {
        uiManager = GetComponent<UIManager>();
    }

    public void Lose()
    {
        GameOver?.Invoke(totalEarnedMoney);

        uiManager.LostGame(totalEarnedMoney);

        FindObjectOfType<InputManager>().enabled = false;
        FindObjectOfType<CarSpawner>().enabled = false;
    }

    public void CarDone(Car car)
    {
        Money += car.NeededServices.Sum(x => x.ServiceReward);

        car.assignedSpot.AssignedCar = null;
        car.assignedSpot = null;

        //drive away
        //on car

        Destroy(car.gameObject);

        CarLeft?.Invoke();

    }

    public void BoughtSpot()
    {
        Money -= SpotUnlockPrice;

        SpotPriceChanged?.Invoke(SpotUnlockPrice + SpotUnlockPriceIncrease);
        spotsUnlocked++;
    }
}
