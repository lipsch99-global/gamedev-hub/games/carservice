﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Progressbar : MonoBehaviour
{
    [SerializeField]
    private Color green = default;

    [SerializeField]
    private Color red = default;

    private Image bar;
    private Image background;

    private ServiceSpot spot;

    private Coroutine blinking;

    public void SetSpot(ServiceSpot serviceSpot)
    {
        bar = transform.GetChild(0).GetChild(0).GetComponent<Image>();
        background = transform.GetChild(0).GetComponent<Image>();

        bar.fillAmount = 0;

        spot = serviceSpot;
        spot.ProgressChanged += UpdateProgressBar;
        spot.SpotUnlocked += SpotUnlocked;
        if (spot.Unlocked == false)
        {
            bar.enabled = false;
            background.enabled = false;
        }

    }

    private void SpotUnlocked(ParkingSpot spot)
    {
        bar.enabled = true;
        background.enabled = true;
    }

    private void UpdateProgressBar(ServiceSpot spot, float Progress)
    {
        bar.fillAmount = Mathf.Clamp01(Progress);
        if (Progress > 1)
        {
            background.color = Color.Lerp(green, red, Progress - 1f);
            if (blinking == null && Progress - 1 > 1)
            {
                blinking = StartCoroutine(StartBlinking());
            }
        }
        else
        {
            background.color = green;
            if (blinking != null)
            {
                StopCoroutine(blinking);
                blinking = null;
                background.gameObject.SetActive(true);
            }
        }
    }

    private IEnumerator StartBlinking()
    {
        float blinkInterval = 1f;

        bool currentState = true;

        while (true)
        {
            currentState = !currentState;
            background.gameObject.SetActive(currentState);
            yield return new WaitForSeconds(blinkInterval);
            blinkInterval *= 0.9f;

            if (blinkInterval < 0.1)
            {
                spot.AssignedCar.Disappear();
                break;
            }
        }
    }
}
