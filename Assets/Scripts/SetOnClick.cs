﻿using UnityEngine;
using UnityEngine.UI;

public class SetOnClick : MonoBehaviour
{

    // Use this for initialization
    private void Start()
    {
        if (Time.time > 5)
        {
            SoundManager sm = FindObjectOfType<SoundManager>();
            GetComponent<Button>().onClick.AddListener(sm.ToggleSound);
        }
    }

}
