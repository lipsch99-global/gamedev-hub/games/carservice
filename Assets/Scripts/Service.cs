﻿using UnityEngine;

[CreateAssetMenu(fileName = "Service", menuName = "Service", order = 1)]
public class Service : ScriptableObject
{
    [SerializeField]
    private string _Name = default;
    public string Name
    {
        get
        {
            return _Name;
        }
    }

    [SerializeField]
    private float _ServiceDuration = default;
    public float ServiceDuration
    {
        get
        {
            return _ServiceDuration;
        }
    }

    [SerializeField]
    private int _ServiceReward = default;
    public int ServiceReward
    {
        get
        {
            return _ServiceReward;
        }
    }

    [SerializeField]
    private int _TimeUntilFailed = default;
    public int TimeUntilFailed
    {
        get
        {
            return _TimeUntilFailed;
        }
    }

    [SerializeField]
    private Sprite _Icon = default;
    public Sprite Icon
    {
        get
        {
            return _Icon;
        }
    }
}
