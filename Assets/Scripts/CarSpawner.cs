﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour
{
    public System.Action CarSpawned;


    [SerializeField]
    private AnimationCurve SpawnInterval = default;

    [SerializeField]
    private GameObject CarPrefab = default;

    private RegularSpot[] arriveSpots;

    private float startTime;

    private RoundManager rm;

    private void Start()
    {
        rm = GetComponent<RoundManager>();

        RegularSpot[] allRegularSpots = FindObjectsOfType<RegularSpot>();
        List<RegularSpot> _arriveSpots = new List<RegularSpot>();
        foreach (var regularSpot in allRegularSpots)
        {
            if (regularSpot.Type == RegularSpot.SpotType.Arrive)
            {
                _arriveSpots.Add(regularSpot);
            }
        }

        arriveSpots = _arriveSpots.ToArray();

        StartCoroutine(SpawnCars());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator SpawnCars()
    {
        startTime = Time.time;

        Service[] allServices = Resources.LoadAll<Service>("Services");

        int howManyServices = HowManyServicesForCar();

        SpawnCar(GetRandomServices(allServices, howManyServices));
        while (true)
        {
            float secondsToWait = SpawnInterval.Evaluate(Time.time - startTime);
            yield return new WaitForSeconds(secondsToWait);

            howManyServices = HowManyServicesForCar();
            SpawnCar(GetRandomServices(allServices, howManyServices));
        }
    }

    private int HowManyServicesForCar()
    {
        float randomNumber = Random.Range(1, 100);

        if (randomNumber < 12.5f)
        {
            return 3;
        }
        else if (randomNumber < 50f)
        {
            return 2;
        }
        else
        {
            return 1;
        }
    }

    private Service[] GetRandomServices(Service[] allServices, int amount)
    {
        if (amount == allServices.Length)
        {
            return allServices;
        }
        else
        {
            Service[] services = new Service[amount];
            int lastNumber = -1;
            for (int i = 0; i < services.Length; i++)
            {
                int index = 0;
                do
                {
                    index = UnityEngine.Random.Range(0, allServices.Length);
                } while (index == lastNumber);
                services[i] = allServices[index];
                lastNumber = index;
            }
            return services;

        }
    }

    private void SpawnCar(Service[] services)
    {
        RegularSpot arriveSpot = GetArriveSpot();
        if (arriveSpot == null)
        {
            rm.Lose();
            return;
        }

        Car car = Instantiate(CarPrefab).GetComponent<Car>();
        car.NeededServices = services;
        arriveSpot.AssignCar(car);

        CarSpawned?.Invoke();
    }


    private RegularSpot GetArriveSpot()
    {
        foreach (var arriveSpot in arriveSpots)
        {
            if (arriveSpot.AssignedCar == null)
            {
                return arriveSpot;
            }
        }
        return null;
    }
}
