﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class ParkingSpot : MonoBehaviour
{
    public Car AssignedCar { get; set; }
    public Action<ParkingSpot> SpotUnlocked;

    [SerializeField]
    private bool _Unlocked;
    public bool Unlocked
    {
        get
        {
            return _Unlocked;
        }
        set
        {
            _Unlocked = value;
        }
    }

    public void UnlockSpot()
    {
        _Unlocked = true;
        SpotUnlocked?.Invoke(this);

        SpriteRenderer constructionSign = transform.GetChild(0).GetComponent<SpriteRenderer>();
        if (constructionSign != null)
        {
            constructionSign.enabled = false;
        }
    }

    public virtual bool CanCarGetDropped(Car car)
    {
        if (_Unlocked == false || AssignedCar != null)
        {
            return false;
        }
        return true;
    }

    public void AssignCar(Car car)
    {
        AssignedCar = car;
        car.transform.position = transform.position;
        car.transform.rotation = transform.rotation;
        if (car.assignedSpot != null)
        {
            car.assignedSpot.AssignedCar = null;
        }
        car.assignedSpot = this;

        CarAssigned();
    }

    public abstract void CarAssigned();
}
