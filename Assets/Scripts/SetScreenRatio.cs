﻿using UnityEngine;

[ExecuteInEditMode]
public class SetScreenRatio : MonoBehaviour
{
    public float widthInUnits = 5f;
    private Camera cam;

    // Use this for initialization
    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    private void Update()
    {
        float ratio = (float)Screen.width / Screen.height * 2;
        cam.orthographicSize = Mathf.Clamp(widthInUnits / ratio, 5, 100);
    }
}
