﻿using UnityEngine;


public class WorldLockedUI : MonoBehaviour
{
    public Vector3 WorldPosition;
    private Camera cam;

    // Use this for initialization
    private void Start()
    {
        cam = FindObjectOfType<Camera>();
    }

    // Update is called once per frame
    private void Update()
    {
        transform.position = cam.WorldToScreenPoint(WorldPosition);
    }
}
