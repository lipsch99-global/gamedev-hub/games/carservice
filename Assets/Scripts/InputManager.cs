﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    private Car draggingCar;
    private Camera cam;

    private void Start()
    {
        cam = Camera.main;
    }

    private void Update()
    {
        StopDragMobile();
        StopDragPc();
        StartDragMobile();
        StartDragPc();

        if (draggingCar != null)
        {
            Vector3 newPos = cam.ScreenToWorldPoint(GetCursorPos());
            newPos.z = draggingCar.transform.position.z;
            draggingCar.transform.position = newPos;
        }



        BuyNewSpotMobile();
        BuyNewSpotPc();

    }

    private void StartDragMobile()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            StartDrag(cam.ScreenToWorldPoint(Input.GetTouch(0).position));
        }

    }
    private void StartDragPc()
    {
        if (Input.GetMouseButtonDown(0) && Input.touchCount == 0)
        {
            StartDrag(cam.ScreenToWorldPoint(Input.mousePosition));
        }
    }
    private void StartDrag(Vector2 pos)
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(pos, cam.transform.forward, 100f);

        foreach (var hit in hits)
        {
            if (hit)
            {
                Car car = hit.transform.GetComponent<Car>();
                if (car != null)
                {
                    draggingCar = car;
                    //draggingCar.GetComponent<Collider2D>().enabled = false;
                    return;
                }
            }
        }
    }

    private void StopDragPc()
    {
        if (draggingCar == null)
        {
            return;
        }
        if (Input.GetMouseButtonUp(0) && Input.touchCount == 0)
        {
            StopDrag(cam.ScreenToWorldPoint(Input.mousePosition));
        }
    }
    private void StopDragMobile()
    {
        if (draggingCar == null)
        {
            return;
        }
        if (Input.touchCount > 0 && (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled))
        {
            StopDrag(cam.ScreenToWorldPoint(Input.GetTouch(0).position));
        }
    }
    private void StopDrag(Vector2 pos)
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(pos, cam.transform.forward, 100f);


        foreach (var hit in hits)
        {
            if (hit)
            {
                ParkingSpot parkingSpot = hit.transform.GetComponent<ParkingSpot>();
                if (parkingSpot != null && parkingSpot.CanCarGetDropped(draggingCar))
                {

                    parkingSpot.AssignCar(draggingCar);
                    //draggingCar.GetComponent<Collider2D>().enabled = true;
                    draggingCar = null;
                    return;
                }
            }
        }
        //draggingCar.GetComponent<Collider2D>().enabled = true;
        draggingCar.transform.position = draggingCar.assignedSpot.transform.position;
        draggingCar = null;


    }

    private Vector2 GetCursorPos()
    {
        if (Input.touchCount > 0)
        {
            return Input.GetTouch(0).position;
        }
        else
        {
            return Input.mousePosition;
        }
    }

    private void BuyNewSpotPc()
    {
        if (Input.GetMouseButtonUp(0) && Input.touchCount == 0)
        {
            BuyNewSpot(cam.ScreenToWorldPoint(Input.mousePosition));
        }
    }
    private void BuyNewSpotMobile()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            BuyNewSpot(cam.ScreenToWorldPoint(Input.GetTouch(0).position));
        }
    }
    private void BuyNewSpot(Vector2 pos)
    {
        RaycastHit2D hit = Physics2D.Raycast(pos, cam.transform.forward);
        if (hit)
        {
            ServiceBuilding serviceBuilding = hit.transform.GetComponent<ServiceBuilding>();
            if (serviceBuilding != null)
            {
                serviceBuilding.UnlockSpot();
                return;
            }
        }
    }
}
