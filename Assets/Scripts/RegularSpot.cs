﻿using UnityEngine;

public class RegularSpot : ParkingSpot
{
    public enum SpotType
    {
        Arrive, Depart, Park
    }

    [SerializeField]
    private SpotType _Type = default;

    public SpotType Type
    {
        get
        {
            return _Type;
        }
    }

    public override bool CanCarGetDropped(Car car)
    {
        bool canGetDropped = base.CanCarGetDropped(car);

        switch (Type)
        {
            case SpotType.Arrive:
                return false;
            case SpotType.Depart:
                if (car.ActiveService >= car.NeededServices.Length)
                {
                    return canGetDropped && true;
                }
                else
                {
                    return false;
                }
            case SpotType.Park:
                return canGetDropped && true;
        }
        return canGetDropped;
    }

    public override void CarAssigned()
    {
        if (Type == SpotType.Depart)
        {
            FindObjectOfType<RoundManager>().CarDone(AssignedCar);
        }
    }
}
