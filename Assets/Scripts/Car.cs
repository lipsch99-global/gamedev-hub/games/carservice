﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Car : MonoBehaviour
{
    [SerializeField]
    private Service[] _NeededServices;
    public Service[] NeededServices
    {
        get
        {
            return _NeededServices;
        }
        set
        {
            _NeededServices = value;
        }
    }

    public int ActiveService { get; private set; }

    [HideInInspector]
    public ParkingSpot assignedSpot = default;

    [SerializeField]
    private SpriteRenderer IconRenderer = default;

    private void Start()
    {
        UpdateNeedIcon();
    }

    private void Update()
    {
        AlignNeedIcon();
    }

    public void ServiceDone(Service service)
    {
        ActiveService++;
        UpdateNeedIcon();
    }

    private void UpdateNeedIcon()
    {
        if (GetNeededService() != null)
        {
            IconRenderer.sprite = GetNeededService().Icon;
        }
        else
        {
            IconRenderer.sprite = null;
        }
    }

    private void AlignNeedIcon()
    {
        IconRenderer.transform.parent.rotation = Quaternion.Euler(Vector3.zero);
    }

    public Service GetNeededService()
    {
        if (ActiveService < _NeededServices.Length)
        {
            return _NeededServices[ActiveService];
        }
        return null;
    }

    public void Disappear()
    {
        assignedSpot.AssignedCar = null;
        assignedSpot = null;
        FindObjectOfType<SoundManager>().PlayCarLeftSound();
        Destroy(gameObject);
    }
}
