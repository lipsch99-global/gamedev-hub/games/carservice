﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource musicPlayer = default;
    [SerializeField]
    private AudioSource soundPlayer = default;
    [SerializeField]
    private AudioClip clickSound = default;
    [SerializeField]
    private AudioClip carSpawnedSound = default;
    [SerializeField]
    private AudioClip carLeftSound = default;
    [SerializeField]
    private AudioClip spotBoughtSound = default;
    [SerializeField]
    private AudioClip loseSound = default;

    private bool soundActive = true;
    private float initialSoundVolume;
    private float initialMusicVolume;

    // Use this for initialization
    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneChange;
        initialSoundVolume = soundPlayer.volume;
        initialMusicVolume = musicPlayer.volume;
    }


    private void ReloadButtons()
    {

        Button[] buttons = Resources.FindObjectsOfTypeAll<Button>();
        foreach (var button in buttons)
        {
            button.onClick.AddListener(PlayClickSound);
        }
    }

    private void OnSceneChange(Scene scene, LoadSceneMode mode)
    {
        ReloadButtons();
        if (scene.buildIndex == 1)
        {
            AddGameEvents();
        }
    }

    private void AddGameEvents()
    {
        FindObjectOfType<CarSpawner>().CarSpawned += PlayCarSpawnedSound;
        var rm = FindObjectOfType<RoundManager>();
        rm.CarLeft += PlayCarLeftSound;
        rm.SpotPriceChanged += PlaySpotBought;
        rm.GameOver += PlayLoseSound;
    }

    private void PlayClickSound()
    {
        PlaySound(clickSound);
    }

    private void PlayCarSpawnedSound()
    {
        PlaySound(carSpawnedSound);
    }

    public void PlayCarLeftSound()
    {
        PlaySound(carLeftSound);
    }

    private void PlaySpotBought(int nonsense)
    {
        PlaySound(spotBoughtSound);
    }

    private void PlayLoseSound(int nonsense)
    {
        PlaySound(loseSound);
    }

    private void PlaySound(AudioClip clip)
    {
        soundPlayer.clip = clip;
        soundPlayer.Play();
    }

    public void ToggleSound()
    {
        if (soundActive)
        {
            soundPlayer.volume = 0;
            musicPlayer.volume = 0;
            soundActive = false;
        }
        else
        {
            soundPlayer.volume = initialSoundVolume;
            musicPlayer.volume = initialMusicVolume;
            soundActive = true;
        }
    }
}
