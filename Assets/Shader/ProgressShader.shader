﻿Shader "Unlit/ProgressShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Progress("Progress", Range(0.0, 1.0)) = 0.5
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha


		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Progress;


			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			float GetCircleFillment(float fillAmount, float2 uv, float circle)
			{
				float realFillAmount = ((fillAmount * 2.0 - 1.0) * -1.0);
				return circle * step(realFillAmount * 360.0, degrees(atan2(uv.x, uv.y)) * 2.0);
			}

			fixed4 frag(v2f i) : SV_Target
			{
				//// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);
				//// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);

				// Normalized pixel coordinates (from 0 to 1)

				float2 uv = i.uv;
				uv = uv * float2(2,2) - float2(1,1);
				uv.x = uv.x * -1.0;
				uv.y = uv.y * -1.0;

				// color definitions
				float3 red = float3(0.905, 0.0298, 0.235);
				float3 yellow = float3(0.945, 0.768, 0.058);
				float3 green = float3(0.179	, 0.796, 0.441);
				float3 darkGreen = float3(0.152, 0.682, 0.376);


				float3 color = green;
				float3 fillmentColor = float3(0.921, 0.937, 0.941);

				// distance from center to uv coordinates
				float uvLength = length(uv);

				// main circle 
				float outerPartOfCircle = step(0.7, uvLength) * step(uvLength,0.9);
				float innerPartOfCircle = step(uvLength,0.55);
				float3 mainCircle = (outerPartOfCircle + innerPartOfCircle) * color;

				//progressbar circle
				float progressBarCircle = step(0.55, uvLength) * step(uvLength,0.7);
				float progressBarCircleUsed = progressBarCircle;


				//empty part of progressbar
				float2 progressBarUv = float2(uv.x * -1.0, uv.y);
				float3 progressBar = GetCircleFillment(1.0 - _Progress,progressBarUv, progressBarCircle) * (color * 0.85);

				//filled part of progressbar
				float3 fillment = GetCircleFillment(_Progress, uv,progressBarCircle) *fillmentColor;

				[flatten] if ((mainCircle + progressBar + fillment).x == 0.0)
				{
					return float4(mainCircle + progressBar + fillment, 0.0);
				}

				float4 fragColor = float4(mainCircle + progressBar + fillment,1.0);

				return fragColor;
				}
				ENDCG
			}
	}
}
